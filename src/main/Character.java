package main;

import java.util.function.BooleanSupplier;

public class Character {

	private Integer health;
	private Integer level;
	private Boolean alive;
	private Integer attack;
	private Integer id;
	private Integer range;
	
	public Character() {
		this.health = 1000;
		this.level = 1;
		this.alive = true;
		this.attack = 100;
		this.range = 2;
		this.id = null;
	}
	
	public Character(Integer id) {
		this.health = 1000;
		this.level = 1;
		this.alive = true;
		this.attack = 100;
		this.range = 2;
		this.id = id;
	}

	public Integer getHealth() {
		return this.health;
	}

	public Integer getLevel() {
		return this.level;
	}

	public Boolean getAlive() {
		return this.alive;
	}
	
	public Integer getRange() {
		return this.range;
	}
	
	public void setLevel(Integer level) {
		this.level = level;
	}
	
	public void setRange(Integer range) {
		this.range = range;
	}

	/**
	 * Recieves damage
	 * @param damage negative damage is treated as healing
	 */
	public void takeDamage(Integer damage) {
		if (this.alive) {
			this.health -= damage;
			
			if (this.health > 1000) {
				this.health = 1000;
			}
			
			if (this.health <= 0) {
				this.health = 0;
				this.alive = false;
			}
		}
	}

	/**
	 * Make damage to another character
	 * @param character target of the damage
	 */
	public void damage(Character character) {
		double mult = 1;
		if (!this.equals(character)) {
			if ((this.level - character.getLevel()) >= 5) {
				mult = 1.5;
			} else if ((character.getLevel() - this.level) >= 5) {
				mult = 0.5;
			}
			character.takeDamage((int) (this.attack * mult));
		}
	}
	
	/**
	 * Heals a character
	 * @param character target of the healing
	 */
	public void heal(Character character) {
		if (this.equals(character)) {
			character.takeDamage(this.attack * -1);
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Character character = (Character) obj;
        return this.id == character.id;
	}
}
