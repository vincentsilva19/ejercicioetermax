package main;

import java.util.ArrayList;

public class Game {

	private static final int rangedType = 20;
	private static final int meleeType = 2;
	
	private ArrayList<Character> characters;
	private ArrayList<Integer[]> coordinates;

	public Game() {
		this.characters = new ArrayList<Character>();
		this.coordinates = new ArrayList<Integer[]>();
		
		for(int i =0; i < 4; i++) {
			this.characters.add(new Character(i));
			Integer[] coord = {i, 0};
			this.coordinates.add(coord);
		}
	} 
	
	public ArrayList<Character> getCharacters() {
		return this.characters;
	}
	
	public void setCharacterLevel(int character, int level) {
		this.characters.get(character).setLevel(level);
	}
	
	public Integer getCharacterCoordinate(int character) {
		return this.coordinates.get(character)[0];
	}
	
	/**
	 * Set the character position on the x axis
	 * @param character character id
	 * @param xCoord coordinate in x axis
	 */
	public void setCharacterCoordinate(int character, int xCoord) {
		Integer[] coordinate = {xCoord, 0};
		this.coordinates.set(character, coordinate);
	}
	/**
	 * Set the range of a character
	 * @param character id of the character
	 * @param type type of combat: "Melee" or "Ranged"
	 */
	public void setCharacterRange(int character, String type) {
		if (type == "Ranged") { 
			this.characters.get(character).setRange(this.rangedType);
		} else if (type == "Melee") {
			this.characters.get(character).setRange(this.meleeType);
		}
	}
	
	/**
	 * Return the distance on the X axis between two characters
	 * @param ch1 first character
	 * @param ch2 second character
	 * @return return the distance rounded
	 */
	public double getDistanceBetween(int ch1, int ch2) {
		Integer[] coord1 = this.coordinates.get(ch1);
		Integer[] coord2 = this.coordinates.get(ch2);
		return Math.abs(coord1[0] - coord2[0]);
	}

	/**
	 * Make fight two characters
	 * @param c1 character that deals the damage
	 * @param c2 character that take the damage
	 */
	public void fight(int c1, int c2) {
		double range = (double)(this.characters.get(c1).getRange());
		double distance = getDistanceBetween(c1, c2);
		
		if (range >= distance) {
			this.characters.get(c1).damage(this.characters.get(c2));
		}	
	}

	/**
	 * Make a character heal another
	 * @param c1 character that will heal
	 * @param c2 character that recieves de healing
	 */
	public void heal(int c1, int c2) {
		this.characters.get(c1).heal(this.characters.get(c2));	
	}
	
	/**
	 * Print a character properties
	 * @param c the index of the character
	 * @return formatted properties
	 */
	public String printCharacter(int c) {
		Character character = this.characters.get(c);
		String status = "Dead";
		String range = "Melee";
		
		if (character.getAlive()) {
			status = "Alive";
		}
		
		if (character.getRange() == this.rangedType) {
			range = "Ranged";
		}
		
		return "<html>Character " + (c+1) + "<br>"
				+ "Health: " + character.getHealth() + "<br>"
				+ "Status: " + status + "<br>"
				+ "Level: " + character.getLevel() + "<br>"
				+ "Type: " + range + "<br>"
				+ "Pos in X: " + this.coordinates.get(c)[0] +"</html>";
	}
}
