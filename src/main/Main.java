package main;

import view.MainWindow;

public class Main {

	public static void main(String[] args) {
		Game game = new Game();
		MainWindow window = new MainWindow(game);
	}

}
