package tests;

import static org.junit.jupiter.api.Assertions.*;
import main.Character;

import org.junit.jupiter.api.Test;

class CharacterTest {
	// A character should ...
	
	private static final int startHealth = 1000;
	private static final int startLevel = 1;
	private static final int startDamage = 100;

	@Test
	void startAt1000Hp() {
		Character character = new Character();
		
		assertEquals(CharacterTest.startHealth, character.getHealth());
	}

	@Test
	void startAtLvl1() {
		Character character = new Character();
		
		assertEquals(CharacterTest.startLevel, character.getLevel());
	}
	
	@Test
	void startAlive() {
		Character character = new Character();
		
		assertTrue(character.getAlive());
	}
	
	@Test
	void beDamaged() {
		Character character = new Character();
		
		character.takeDamage(50);
		
		assertEquals(CharacterTest.startHealth - 50, character.getHealth());
	}
	
	@Test
	void dieWithFatalDamage() {
		Character character = new Character();
		
		character.takeDamage(CharacterTest.startHealth);
		
		assertFalse(character.getAlive());
	}
	
	@Test
	void beHealed() {
		Character character = new Character();
		
		character.takeDamage(400);
		character.takeDamage(-50);
		
		assertEquals(650, character.getHealth());
	}
	
	@Test
	void limitHealTo1000() {
		Character character = new Character();
		
		character.takeDamage(-100);
		
		assertEquals(CharacterTest.startHealth, character.getHealth());
	}
	
	@Test
	void stayDeadAfterHeal() {
		Character character = new Character();
		
		character.takeDamage(CharacterTest.startHealth);
		character.takeDamage(-10);
		
		assertFalse(character.getAlive());
		assertEquals(0, character.getHealth());
	}
	
	@Test
	void damageAnotherCharacter() {
		Character ch1 = new Character(1);
		Character ch2 = new Character(2);
		Integer expectedHealth = (CharacterTest.startHealth - CharacterTest.startDamage);
		
		ch1.damage(ch2);
		
		assertEquals(expectedHealth, ch2.getHealth());
	}
	
	@Test
	void damageAnotherCharacterWeaker() {
		Character ch1 = new Character(1);
		Character ch2 = new Character(2);
		Integer expectedHealth = (CharacterTest.startHealth - (int) (CharacterTest.startDamage * 1.5));
		
		ch1.setLevel(6);
		ch1.damage(ch2);
		
		assertEquals(expectedHealth, ch2.getHealth());
	}
	
	@Test
	void damageAnotherCharacterStronger() {
		Character ch1 = new Character(1);
		Character ch2 = new Character(2);
		Integer expectedHealth = (CharacterTest.startHealth - (int) (CharacterTest.startDamage * 0.5));
		
		ch2.setLevel(6);
		ch1.damage(ch2);
		
		assertEquals(expectedHealth, ch2.getHealth());
	}
	
	@Test
	void NotDamageToItself() {
		Character ch1 = new Character();
		Integer expectedHealth = CharacterTest.startHealth;
		
		ch1.damage(ch1);
		
		assertEquals(expectedHealth, ch1.getHealth());
	}
	
	@Test
	void healItself() {
		Character ch1 = new Character();
		Integer expectedHealth = CharacterTest.startHealth;
		
		ch1.takeDamage(100);
		ch1.heal(ch1);
		
		assertEquals(expectedHealth, ch1.getHealth());
	}
	
	@Test
	void NotHealAnotherCharacter() {
		Character ch1 = new Character(1);
		Character ch2 = new Character(2);
		Integer expectedHealth = CharacterTest.startHealth;
		
		ch2.takeDamage(CharacterTest.startDamage);
		ch1.heal(ch2);
		
		assertNotEquals(expectedHealth, ch2.getHealth());
	}
	
	@Test
	void knowWhenItsTheSameCharacter() {
		Character ch1 = new Character();
		
		assertTrue(ch1.equals(ch1));
	}
	
	@Test
	void knowWhenItsADifferentCharacter() {
		Character ch1 = new Character(1);
		Character ch2 = new Character(2);
		
		assertFalse(ch1.equals(ch2));
	}
}
