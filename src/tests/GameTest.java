package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import main.Game;
import main.Character;
import static org.mockito.Mockito.*;

class GameTest {
	// A game should ...
	
	@Test
	void startWith4Characters() {
		Game game = new Game();
				
		assertEquals(4, game.getCharacters().size());
	}

	@Test
	void makeFightOneToAnotherWithCorrectRange() {
		Game game = new Game();
		Integer startHealth = game.getCharacters().get(1).getHealth();
		
		game.setCharacterCoordinate(0, 10);
		game.setCharacterCoordinate(1, 11);
		game.fight(0, 1);
		
		assertNotEquals(startHealth, game.getCharacters().get(1).getHealth());
	}
	
	@Test
	void notMakeAFightWithIncorrectRange() {
		Game game = new Game();
		Integer startHealth = game.getCharacters().get(2).getHealth();
		
		game.setCharacterCoordinate(0, 50);
		game.fight(0, 2);
		
		assertEquals(startHealth, game.getCharacters().get(2).getHealth());
	}
	
	@Test
	void makeHealOneToAnother() {
		Game game = new Game();
		Integer startHealth = game.getCharacters().get(2).getHealth();

		game.fight(0, 2);
		game.heal(2, 2);

		assertEquals(startHealth, game.getCharacters().get(2).getHealth());
	}
	
	@Test
	void printCharacterProperties() {
		Game game = new Game();
		String result;
		String expects;
		
		result = game.printCharacter(0);
		expects = "<html>Character 1<br>"
				+ "Health: 1000<br>"
				+ "Status: Alive<br>"
				+ "Level: 1<br>"
				+ "Type: Melee<br>"
				+ "Pos in X: 0</html>";

		assertEquals(expects, result);
	}
	
	
	@Test
	void keepCharacterCoordinates() {
		Game game = new Game();
		
		assertEquals(0, game.getCharacterCoordinate(0));
	}

	@Test
	void setLevelOfACharacter() {
		Game game = new Game();
		
		game.setCharacterLevel(0,3);
		
		assertEquals(3, game.getCharacters().get(0).getLevel());
	}
	
	@Test
	void setCoordinateOfACharacter() {
		Game game = new Game();
		
		game.setCharacterCoordinate(0, 10);
		
		assertEquals(10, game.getCharacterCoordinate(0));
	}
	
	@Test
	void setRangeOfACharacterRanged() {
		Game game = new Game();
		
		game.setCharacterRange(0, "Ranged");
		
		assertEquals(20, game.getCharacters().get(0).getRange());
	}
	
	@Test
	void setRangeOfACharacterMelee() {
		Game game = new Game();
		
		game.setCharacterRange(0, "Melee");
		
		assertEquals(2, game.getCharacters().get(0).getRange());
	}
	
	@Test
	void getCharacterDistanceOfAnother() {
		Game game = new Game();
		
		game.setCharacterCoordinate(0, 10);
		game.setCharacterCoordinate(1, 1);
		
		assertEquals(9.0, game.getDistanceBetween(0,1));
	}
}
