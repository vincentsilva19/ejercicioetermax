package view;

import java.awt.EventQueue;
import java.awt.Window;

import javax.swing.JFrame;

import main.Game;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class MainWindow {

	private JFrame frame;
	private Game game;
	private JLabel lblChar1;
	private JLabel lblChar3;
	private JLabel lblChar2;
	private JLabel lblChar4;
	private JButton btnHeal;
	private JButton btnSetLevel;
	private JButton btnSetRange;
	private JButton setRange;

	/**
	 * Create the application.
	 */
	public MainWindow(Game game) {
		this.game = game;
		initialize();
		updateWindow();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 900, 469);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		lblChar1 = new JLabel("test");
		lblChar1.setFont(new Font("Dialog", Font.BOLD, 16));
		lblChar1.setBounds(15, 12, 175, 248);
		frame.getContentPane().add(lblChar1);
		
		lblChar2 = new JLabel((String) null);
		lblChar2.setFont(new Font("Dialog", Font.BOLD, 16));
		lblChar2.setBounds(205, 12, 175, 248);
		frame.getContentPane().add(lblChar2);
		
		lblChar3 = new JLabel((String) null);
		lblChar3.setFont(new Font("Dialog", Font.BOLD, 16));
		lblChar3.setBounds(395, 12, 175, 248);
		frame.getContentPane().add(lblChar3);
		
		lblChar4 = new JLabel((String) null);
		lblChar4.setFont(new Font("Dialog", Font.BOLD, 16));
		lblChar4.setBounds(585, 12, 175, 248);
		frame.getContentPane().add(lblChar4);
		
		JButton btnAttack = new JButton("Attack");
		btnAttack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				beginFight();
			}
		});
		btnAttack.setBounds(36, 347, 114, 25);
		frame.getContentPane().add(btnAttack);
		
		btnHeal = new JButton("Heal");
		btnHeal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				beginHeal();
			}
		});
		btnHeal.setBounds(186, 347, 114, 25);
		frame.getContentPane().add(btnHeal);
		
		btnSetLevel = new JButton("Set level");
		btnSetLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setLevel();
			}
		});
		btnSetLevel.setBounds(336, 347, 114, 25);
		frame.getContentPane().add(btnSetLevel);
		
		btnSetRange = new JButton("Move on X");
		btnSetRange.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setPos();
			}
		});
		btnSetRange.setBounds(486, 347, 114, 25);
		frame.getContentPane().add(btnSetRange);
		
		setRange = new JButton("Set range");
		setRange.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setRange();
			}
		});
		setRange.setBounds(636, 347, 114, 25);
		frame.getContentPane().add(setRange);
		frame.setVisible(true);
	}
	
	private void updateWindow() {
		lblChar1.setText(this.game.printCharacter(0));
		lblChar2.setText(this.game.printCharacter(1));
		lblChar3.setText(this.game.printCharacter(2));
		lblChar4.setText(this.game.printCharacter(3));
	}
	
	private void beginFight() {
		Integer[] choices = {1, 2, 3, 4};
	    Integer source = (Integer) JOptionPane.showInputDialog(null, "", "Who makes an attack?", JOptionPane.QUESTION_MESSAGE, null, choices, choices[0]);
	    Integer target = (Integer) JOptionPane.showInputDialog(null, "", "To?", JOptionPane.QUESTION_MESSAGE, null, choices, choices[0]);
	    
	    if (source != null && target != null) {
    		this.game.fight(source -1 , target -1);
	    	updateWindow();
	    }
	}
	
	private void beginHeal() {
		Integer[] choices = {1, 2, 3, 4};
	    Integer source = (Integer) JOptionPane.showInputDialog(null, "", "Who heals?", JOptionPane.QUESTION_MESSAGE, null, choices, choices[0]);
	    Integer target = (Integer) JOptionPane.showInputDialog(null, "", "To whom?", JOptionPane.QUESTION_MESSAGE, null, choices, choices[0]);
	    
	    if (source != null && target != null) {
    		this.game.heal(source -1 , target -1);
	    	updateWindow();
	    }
	}
	
	private void setLevel() {
		Integer[] choices = {1, 2, 3, 4};
	    Integer source = (Integer) JOptionPane.showInputDialog(null, "", "To whom?", JOptionPane.QUESTION_MESSAGE, null, choices, choices[0]);
	    Integer level = Integer.parseInt(JOptionPane.showInputDialog(null, "", "To wich level?", JOptionPane.QUESTION_MESSAGE));
	    
	    if (source != null && level != null && level > 0) {
	    	this.game.setCharacterLevel(source - 1, level);
	    	updateWindow();
	    }
	}
	
	private void setPos() {
		Integer[] choices = {1, 2, 3, 4};
	    Integer source = (Integer) JOptionPane.showInputDialog(null, "", "To whom?", JOptionPane.QUESTION_MESSAGE, null, choices, choices[0]);
	    Integer pos = Integer.parseInt(JOptionPane.showInputDialog(null, "", "To wich x coordinate?", JOptionPane.QUESTION_MESSAGE));
	    
	    if (source != null && pos != null && pos > 0) {
	    	this.game.setCharacterCoordinate(source - 1, pos);
	    	updateWindow();
	    }
	}
	
	private void setRange() {
		Integer[] characters = {1, 2, 3, 4};
		String[] types = {"Melee", "Ranged"};
	    Integer source = (Integer) JOptionPane.showInputDialog(null, "", "To whom?", JOptionPane.QUESTION_MESSAGE, null, characters, characters[0]);
	    String range = (String) JOptionPane.showInputDialog(null, "", "To wich type?", JOptionPane.QUESTION_MESSAGE, null, types, types[0]);
	    
	    if (source != null && range != null) {
	    	this.game.setCharacterRange(source - 1, range);
	    	updateWindow();
	    }
	}
}
